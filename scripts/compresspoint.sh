#!/bin/bash

bench=$1
path=$2
cmd=$3
BBVFile=$4
temp=/tmp/$bench
mkdir $bench
echo $PWD
OLD_PWD=$PWD
gnome-terminal -e "./sleep.sh $temp $bench" &
cd $path
#echo "$OLD_PWD/../pinplay-drdebug-pldi2016-3.0-pin-3.0-76991-gcc-linux/pin -follow_execv -t $OLD_PWD/../pinplay-drdebug-pldi2016-3.0-pin-3.0-76991-gcc-linux/source/tools/SimpleExamples/obj-intel64/CompressPoint.so -c 1 -temp $temp -o $OLD_PWD/CompressPoint2.data -- $cmd"
$OLD_PWD/../pinplay-drdebug-pldi2016-3.0-pin-3.0-76991-gcc-linux/pin -follow_execv -t $OLD_PWD/../pinplay-drdebug-pldi2016-3.0-pin-3.0-76991-gcc-linux/source/tools/SimpleExamples/obj-intel64/CompressPoint.so -c 1 -temp $temp -o $OLD_PWD/CompressPoint2.data -- $cmd
cd -
kill -9 `pgrep sleep`
python get_compresspointVectors.py $bench/$bench-user.out > $bench/vectors.bb
../SimPoint.3.2/bin/simpoint -loadCompFVFile $bench/vectors.bb -loadFVFile $BBVFile -k 1 -saveSimpoints $bench/results.simpts -saveSimpointWeights $bench/results.weights
