/*BEGIN_LEGAL 
Intel Open Source License 

Copyright (c) 2002-2014 Intel Corporation. All rights reserved.
 
Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:

Redistributions of source code must retain the above copyright notice,
this list of conditions and the following disclaimer.  Redistributions
in binary form must reproduce the above copyright notice, this list of
conditions and the following disclaimer in the documentation and/or
other materials provided with the distribution.  Neither the name of
the Intel Corporation nor the names of its contributors may be used to
endorse or promote products derived from this software without
specific prior written permission.
 
THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE INTEL OR
ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
END_LEGAL */
//
// @ORIGINAL_AUTHOR: Artur Klauser
// @EXTENDED: Rodric Rabbah (rodric@gmail.com) 
//

/*! @file
 *  This file contains an ISA-portable cache simulator
 *  data cache hierarchies
 */


#include "pin.H"

#include <iostream>
#include <fstream>

#include "esha_dcache.H"
#include "pin_profile.H"

std::ofstream outFile;

/* ===================================================================== */
/* Commandline Switches */
/* ===================================================================== */

KNOB<string> KnobOutputFile(KNOB_MODE_WRITEONCE,    "pintool",
    "o", "esha_dcache.out", "specify dcache file name");
KNOB<BOOL>   KnobTrackLoads(KNOB_MODE_WRITEONCE,    "pintool",
    "tl", "0", "track individual loads -- increases profiling time");
KNOB<BOOL>   KnobTrackStores(KNOB_MODE_WRITEONCE,   "pintool",
   "ts", "0", "track individual stores -- increases profiling time");
KNOB<UINT32> KnobThresholdHit(KNOB_MODE_WRITEONCE , "pintool",
   "rh", "100", "only report memops with hit count above threshold");
KNOB<UINT32> KnobThresholdMiss(KNOB_MODE_WRITEONCE, "pintool",
   "rm","100", "only report memops with miss count above threshold");
KNOB<UINT32> KnobCacheSizeL1(KNOB_MODE_WRITEONCE, "pintool",
    "c1","32", "cache size in kilobytes");
KNOB<UINT32> KnobLineSizeL1(KNOB_MODE_WRITEONCE, "pintool",
    "b1","32", "cache block size in bytes");
KNOB<UINT32> KnobAssociativityL1(KNOB_MODE_WRITEONCE, "pintool",
    "a1","4", "cache associativity (1 for direct mapped)");
KNOB<UINT32> KnobCacheSizeL2(KNOB_MODE_WRITEONCE, "pintool",
    "c2","2048", "cache size in kilobytes");
KNOB<UINT32> KnobLineSizeL2(KNOB_MODE_WRITEONCE, "pintool",
    "b2","64", "cache block size in bytes");
KNOB<UINT32> KnobAssociativityL2(KNOB_MODE_WRITEONCE, "pintool",
    "a2","16", "cache associativity (1 for direct mapped)");

/* ===================================================================== */
/* Print Help Message                                                    */
/* ===================================================================== */

INT32 Usage()
{
    cerr <<
        "This tool represents a cache simulator.\n"
        "\n";

    cerr << KNOB_BASE::StringKnobSummary() << endl; 
    return -1;
}

/* ===================================================================== */
/* Global Variables */
/* ===================================================================== */

UINT32 esha_access = 0;
UINT32 mainmem_accesses=0;

// wrap configuation constants into their own name space to avoid name clashes
namespace DL1
{
    const UINT32 max_sets = KILO; // cacheSize / (lineSize * associativity);
    const UINT32 max_associativity = 8; // associativity;
    const CACHE_ALLOC::STORE_ALLOCATION allocation = CACHE_ALLOC::STORE_ALLOCATE;

    typedef CACHE_LRU(max_sets, max_associativity, allocation) CACHE;
}

DL1::CACHE* dl1 = NULL;

namespace DL2
{
    const UINT32 max_sets = 4*KILO; // cacheSize / (lineSize * associativity);
    const UINT32 max_associativity = 16; // associativity;
    const CACHE_ALLOC::STORE_ALLOCATION allocation = CACHE_ALLOC::STORE_ALLOCATE;

    typedef CACHE_LRU(max_sets, max_associativity, allocation) CACHE;
}

DL2::CACHE* dl2 = NULL;

typedef enum
{
    COUNTER_MISS = 0,
    COUNTER_HIT = 1,
    COUNTER_NUM
} COUNTER;



typedef  COUNTER_ARRAY<UINT64, COUNTER_NUM> COUNTER_HIT_MISS;


// holds the counters with misses and hits
// conceptually this is an array indexed by instruction address
COMPRESSOR_COUNTER<ADDRINT, UINT32, COUNTER_HIT_MISS> profileL1;
COMPRESSOR_COUNTER<ADDRINT, UINT32, COUNTER_HIT_MISS> profileL2;

/* ===================================================================== */

VOID LoadMulti(ADDRINT addr, UINT32 size, UINT32 instId1, UINT32 instId2)
{
    // first level D-cache
    const BOOL dl1Hit = dl1->Access(addr, size, ACCESS_TYPE_LOAD);

    const COUNTER counter1 = dl1Hit ? COUNTER_HIT : COUNTER_MISS;
    profileL1[instId1][counter1]++;
    
    if(!dl1Hit) // On L1 miss, access from L2
    {
    	const BOOL dl2Hit = dl2->Access(addr, size, ACCESS_TYPE_LOAD);
    	const COUNTER counter2 = dl2Hit ? COUNTER_HIT : COUNTER_MISS;
    	profileL2[instId2][counter2]++;
    }
    esha_access++;
}

/* ===================================================================== */

VOID StoreMulti(ADDRINT addr, UINT32 size, UINT32 instId1, UINT32 instId2)
{
    // first level D-cache
    const BOOL dl1Hit = dl1->Access(addr, size, ACCESS_TYPE_STORE);

    const COUNTER counter1 = dl1Hit ? COUNTER_HIT : COUNTER_MISS;
    profileL1[instId1][counter1]++;
    
    if(!dl1Hit) // On L1 miss, access from L2
    {
    	const BOOL dl2Hit = dl2->Access(addr, size, ACCESS_TYPE_STORE);
    	const COUNTER counter2 = dl2Hit ? COUNTER_HIT : COUNTER_MISS;
    	profileL2[instId2][counter2]++;
    }
    esha_access++;
}

/* ===================================================================== */

VOID LoadSingle(ADDRINT addr, UINT32 instId1, UINT32 instId2)
{
    // @todo we may access several cache lines for 
    // first level D-cache
    ADDRINT replaced;
    const BOOL dl1Hit = dl1->AccessSingleLine(addr, ACCESS_TYPE_LOAD, &replaced);

    const COUNTER counter = (dl1Hit==HIT) ? COUNTER_HIT : COUNTER_MISS;
    profileL1[instId1][counter]++;
    
    if(dl1Hit!=HIT) // On L1 miss, access from L2
    {
        if(dl1Hit == MISS_WITH_WB)
            dl2->UpdateDirtyBit(replaced);
    	const BOOL dl2Hit = dl2->AccessSingleLine(addr, ACCESS_TYPE_LOAD, &replaced);
    	const COUNTER counter2 = (dl2Hit==HIT) ? COUNTER_HIT : COUNTER_MISS;
    	profileL2[instId2][counter2]++;
    	if(dl2Hit == MISS_WITH_WB)
    	    mainmem_accesses++;
    }
    esha_access++;
}
/* ===================================================================== */

VOID StoreSingle(ADDRINT addr, UINT32 instId1, UINT32 instId2)
{
    // @todo we may access several cache lines for 
    // first level D-cache
    ADDRINT replaced;
    const ACCESS_RESULT_TYPE dl1Hit = dl1->AccessSingleLine(addr, ACCESS_TYPE_STORE, &replaced);

    const COUNTER counter = (dl1Hit==HIT) ? COUNTER_HIT : COUNTER_MISS;
    profileL1[instId2][counter]++;
        
    if(dl1Hit!=HIT) // On L1 miss, access from L2
    {
        if(dl1Hit == MISS_WITH_WB)
            dl2->UpdateDirtyBit(replaced);
    	const BOOL dl2Hit = dl2->AccessSingleLine(addr, ACCESS_TYPE_STORE, &replaced);
    	const COUNTER counter2 = (dl2Hit==HIT) ? COUNTER_HIT : COUNTER_MISS;
    	profileL2[instId2][counter2]++;
    	if(dl2Hit == MISS_WITH_WB)
    	    mainmem_accesses++;
    }
    esha_access++;
}

/* ===================================================================== */

VOID LoadMultiFast(ADDRINT addr, UINT32 size)
{
    const bool dl1Hit = dl1->Access(addr, size, ACCESS_TYPE_LOAD);
    
    if(!dl1Hit)
    	dl2->Access(addr, size, ACCESS_TYPE_LOAD);
}

/* ===================================================================== */

VOID StoreMultiFast(ADDRINT addr, UINT32 size)
{
    const BOOL dl1Hit = dl1->Access(addr, size, ACCESS_TYPE_STORE);
    if(!dl1Hit)
    	dl2->Access(addr, size, ACCESS_TYPE_STORE);
}

/* ===================================================================== */

VOID LoadSingleFast(ADDRINT addr)
{
    ADDRINT replaced;
    const BOOL dl1Hit = dl1->AccessSingleLine(addr, ACCESS_TYPE_LOAD, &replaced);

    if(dl1Hit!=HIT) // On L1 miss, access from L2
    {
        if(dl1Hit == MISS_WITH_WB)
            dl2->UpdateDirtyBit(replaced);
    	const BOOL dl2Hit = dl2->AccessSingleLine(addr, ACCESS_TYPE_LOAD, &replaced);
    	if(dl2Hit == MISS_WITH_WB)
    	    mainmem_accesses++;
    }
}

/* ===================================================================== */

VOID StoreSingleFast(ADDRINT addr)
{
    ADDRINT replaced;
    const ACCESS_RESULT_TYPE dl1Hit = dl1->AccessSingleLine(addr, ACCESS_TYPE_STORE, &replaced);
        
    if(dl1Hit!=HIT) // On L1 miss, access from L2
    {
        if(dl1Hit == MISS_WITH_WB)
            dl2->UpdateDirtyBit(replaced);
    	const BOOL dl2Hit = dl2->AccessSingleLine(addr, ACCESS_TYPE_STORE, &replaced);
    	if(dl2Hit == MISS_WITH_WB)
    	    mainmem_accesses++;
    }
}



/* ===================================================================== */

VOID Instruction(INS ins, void * v)
{
    if (INS_IsMemoryRead(ins))
    {
        // map sparse INS addresses to dense IDs
        const ADDRINT iaddr = INS_Address(ins);
        const UINT32 instId1 = profileL1.Map(iaddr);
	const UINT32 instId2 = profileL2.Map(iaddr);
	
        const UINT32 size = INS_MemoryReadSize(ins);
        const BOOL   single = (size <= 4);
                
        if( KnobTrackLoads )
        {
            if( single )
            {
                INS_InsertPredicatedCall(
                    ins, IPOINT_BEFORE, (AFUNPTR) LoadSingle,
                    IARG_MEMORYREAD_EA,
                    IARG_UINT32, instId1,
                    IARG_UINT32, instId2,
                    IARG_END);
            }
            else
            {
                INS_InsertPredicatedCall(
                    ins, IPOINT_BEFORE,  (AFUNPTR) LoadMulti,
                    IARG_MEMORYREAD_EA,
                    IARG_MEMORYREAD_SIZE,
                    IARG_UINT32, instId1,
                    IARG_UINT32, instId2,
                    IARG_END);
            }
                
        }
        else
        {
            if( single )
            {
                INS_InsertPredicatedCall(
                    ins, IPOINT_BEFORE,  (AFUNPTR) LoadSingleFast,
                    IARG_MEMORYREAD_EA,
                    IARG_END);
                        
            }
            else
            {
                INS_InsertPredicatedCall(
                    ins, IPOINT_BEFORE,  (AFUNPTR) LoadMultiFast,
                    IARG_MEMORYREAD_EA,
                    IARG_MEMORYREAD_SIZE,
                    IARG_END);
            }
        }
    }
        
    if ( INS_IsMemoryWrite(ins) )
    {
        // map sparse INS addresses to dense IDs
        const ADDRINT iaddr = INS_Address(ins);
        const UINT32 instId1 = profileL1.Map(iaddr);
        const UINT32 instId2 = profileL2.Map(iaddr);
            
        const UINT32 size = INS_MemoryWriteSize(ins);

        const BOOL   single = (size <= 4);
                
        if( KnobTrackStores )
        {
            if( single )
            {
                INS_InsertPredicatedCall(
                    ins, IPOINT_BEFORE,  (AFUNPTR) StoreSingle,
                    IARG_MEMORYWRITE_EA,
                    IARG_UINT32, instId1,
                    IARG_UINT32, instId2,
                    IARG_END);
            }
            else
            {
                INS_InsertPredicatedCall(
                    ins, IPOINT_BEFORE,  (AFUNPTR) StoreMulti,
                    IARG_MEMORYWRITE_EA,
                    IARG_MEMORYWRITE_SIZE,
                    IARG_UINT32, instId1,
                    IARG_UINT32, instId2,
                    IARG_END);
            }
                
        }
        else
        {
            if( single )
            {
                INS_InsertPredicatedCall(
                    ins, IPOINT_BEFORE,  (AFUNPTR) StoreSingleFast,
                    IARG_MEMORYWRITE_EA,
                    IARG_END);
                        
            }
            else
            {
                INS_InsertPredicatedCall(
                    ins, IPOINT_BEFORE,  (AFUNPTR) StoreMultiFast,
                    IARG_MEMORYWRITE_EA,
                    IARG_MEMORYWRITE_SIZE,
                    IARG_END);
            }
        }
            
    }
}

/* ===================================================================== */

VOID Fini(int code, VOID * v)
{
    // print D-cache profile
    // @todo what does this print
    
    outFile << "PIN:MEMLATENCIES 1.0. 0x0\n";
            
    outFile <<
        "#\n"
        "# DCACHE L1 stats\n"
        "#\n";
    
    outFile << dl1->StatsLong("# ", CACHE_BASE::CACHE_TYPE_DCACHE);
    
    outFile <<
        "#\n"
        "# DCACHE L2 stats\n"
        "#\n";
    
    outFile << dl2->StatsLong("# ", CACHE_BASE::CACHE_TYPE_DCACHE);
    
    outFile << "\nLeftover Writebacks in L1 are " <<dl1->CountDirty()<<"\n";
    
    outFile << "\nLeftover Writebacks in L2 are " <<dl2->CountDirty()<<"\n";

    outFile << "\nExtra main memory Access are " <<mainmem_accesses<<"\n";

    if( KnobTrackLoads || KnobTrackStores ) {
        outFile <<
            "#\n"
            "# LOAD stats from L1\n"
            "#\n";
        
        outFile << profileL1.StringLong();
        outFile <<
            "#\n"
            "# LOAD stats from L2\n"
            "#\n";
        
        outFile << profileL2.StringLong();
        outFile << "\nEsha Access" << esha_access;
        outFile << "\nExtra main memory Access are " <<mainmem_accesses<<"\n";
        
    }
    outFile.close();
}

/* ===================================================================== */
/* Main                                                                  */
/* ===================================================================== */

int main(int argc, char *argv[])
{
    PIN_InitSymbols();

    if( PIN_Init(argc,argv) )
    {
        return Usage();
    }

    outFile.open(KnobOutputFile.Value().c_str());

/*    dl1 = new DL1::CACHE("L1 Data Cache", 
                         KnobCacheSizeL1.Value() * KILO,
                         KnobLineSizeL1.Value(),
                         KnobAssociativityL1.Value());*/
    dl2 = new DL2::CACHE("L2 Data Cache", 
                         KnobCacheSizeL2.Value() * KILO,
                         KnobLineSizeL2.Value(),
                         KnobAssociativityL2.Value());
    
    profileL1.SetKeyName("iaddr          ");
    profileL1.SetCounterName("dcache:miss        dcache:hit");
    
    profileL2.SetKeyName("iaddr          ");
    profileL2.SetCounterName("dcache:miss        dcache:hit");

    COUNTER_HIT_MISS threshold;

    threshold[COUNTER_HIT] = KnobThresholdHit.Value();
    threshold[COUNTER_MISS] = KnobThresholdMiss.Value();
    
    profileL1.SetThreshold( threshold );
    profileL2.SetThreshold( threshold );
    
    INS_AddInstrumentFunction(Instruction, 0);
    PIN_AddFiniFunction(Fini, 0);

    // Never returns

    PIN_StartProgram();
    
    return 0;
}

/* ===================================================================== */
/* eof */
/* ===================================================================== */
