from decimal import *
import os
import shutil
import sys
import glob


#Print histogram with buckets of compression rtio from 0 to 120 in steps of 0.1

#Also counts the #pages needed and the effective memory increase`

old=0
with open(str(sys.argv[1])) as fp:
	fp.readline()
	for line in fp:
		words=line.split()
		print str(int(words[0]) - old)
		old=int(words[0])
